// ==UserScript==
// @name         Images from Google books preview
// @namespace    https://www.u-m.me/
// @version      0.2.1
// @description  Provides a button to start capturing pages and download them as a pdf.
// @author       Umesh Mohan (moh@nume.sh)
// @match        https://books.google.co.in/books*
// @grant        none
// @require      https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js
// ==/UserScript==
images_ = {};
var book_section = {};
var book_sections = [];
var page_numbers = [];
var link;

function updateImages() {
    var current_images = viewport.getElementsByTagName("img");
    for (var image_number in current_images) {
        var image_src = current_images[image_number].src;
        if (image_src == undefined || image_src == '') {continue;}
        var image_parameters = JSON.parse('{"' + image_src.split('?')[1].replace(/&/g, '","').replace(/=/g,'":"') + '"}');
        if ('pg' in image_parameters) {
            if (image_parameters.pg in images_) {
                if (+image_parameters.w > images_[image_parameters.pg].w) {
                    images_[image_parameters.pg].w = +image_parameters.w;
                    downloadImage(image_src, image_parameters.pg);
                }
            } else {
                images_[image_parameters.pg] = {'w': +image_parameters.w};
                downloadImage(image_src, image_parameters.pg);
            }
        }
    }
}

function updateSectionLists(page_number) {
    var current_section = page_number.substring(0, 2);
    if (current_section in book_section) {
        book_section[current_section].push(Number(page_number.substring(2)));
        book_section[current_section] = book_section[current_section].sort();
    } else {
        book_sections.push(current_section);
        book_sections = book_sections.sort();
        book_section[current_section] = [Number(page_number.substring(2))];
    }
}

function downloadImage(image_src, page_number) {
    var img = new Image();
    img.onload = function() {
        var canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        var context = canvas.getContext('2d');
        context.drawImage(img, 0, 0);
        images_[page_number].data_url = canvas.toDataURL('image/jpeg', 1);
        images_[page_number].width = img.width;
        images_[page_number].height = img.height;
        updateSectionLists(page_number);
    };
    img.src = image_src;
}

function updatePageNumbersList() {
    page_numbers = [];
    for (var current_section_number in book_sections) {
        var current_section = book_sections[current_section_number];
        for (var current_page_number in book_section[current_section]) {
            page_numbers.push(current_section + book_section[current_section][current_page_number]);
        }
    }
}

function saveImagesAsPDF() {
    updatePageNumbersList();
    var doc = new jsPDF();
    for(var page_number in page_numbers) {
        var page = images_[page_numbers[page_number]];
        var wratio = 210 / page.width;
        var hratio = 297 / page.height;
        var ratio = 0;
        if (wratio < hratio) {
            ratio = wratio;
        } else {
            ratio = hratio;
        }
        var iw = page.width * ratio;
        var ih = page.height * ratio;
        var x = (210 - iw) / 2;
        var y = (297 - ih) / 2;
        doc.addPage();
        doc.addImage(page.data_url, 'jpeg', x, y, iw, ih);
    }
    doc.save(document.title + '.pdf');
    link.remove();
    setupHack();
}

function setupHack() {
    link = document.createElement("a");
    link.innerHTML = " 😇";
    link.style.cursor = "pointer";
    link.onclick = function() {
        link.remove();
        viewport = document.getElementById("viewport");
        viewport.addEventListener("DOMSubtreeModified",updateImages,false);
        updateImages();
        link = document.createElement("a");
        link.innerHTML = " 💾PDF";
        link.style.cursor = "pointer";
        link.onclick = saveImagesAsPDF;
        document.getElementsByClassName("kd-appname")[0].appendChild(link);
    };
    document.getElementsByClassName("kd-appname")[0].appendChild(link);
}

setupHack();